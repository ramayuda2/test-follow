from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.contrib.auth import authenticate
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import login as auth_login
from django.contrib import messages
from .form import Register


def Login(request):
    if request.user.is_authenticated:
        return redirect("/")
    if request.method == "POST":
        get_request = request.POST
        auth = authenticate(
            username=get_request["username"], password=get_request["password"]
        )
        if auth is not None:
            auth_login(request, auth)
            return redirect('home')
        else:
            return redirect('/login')
    return render(request, 'login.html')

def Logout(request):
    auth_logout(request)
    return redirect("/login")


def register_request(request):
    form = Register()
    if request.user.is_authenticated:
        return redirect("/")
    if request.method == "POST":
        form = Register(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Success Registration')
            return redirect('/login')
        messages.error(request, 'Gagal Regsiter')
    return render(request, 'register.html', {'form': form})