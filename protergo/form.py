from django.contrib.auth.forms import UserCreationForm
from account.models.Account import Profile
from django import forms

class Register(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = Profile
        fields = ("username", "email", "password1", "password2", 'nama_lengkap')

    def save(self, commit=True):
        user = super(Register, self).save(commit=False)
        user.is_staff = True
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user
