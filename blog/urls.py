from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from . import views

urlpatterns = [
    path('', views.HomeView , name='home'),
    path('edit_post/<int:id>', views.edit_post, name='edit_post'),
    path('delete/<int:id>', views.delete_post, name='delete_post'),
    path('BlogCreate', views.CreateNewPostView, name='create'),
    path('Detail-post/<int:id>', views.DetailPostView, name='detail'),
    path('profile/<int:id>', views.ProfileView, name='profile')
]