from django.db import models
from account.models.Account import Profile


class Blog(models.Model):
    Title = models.CharField(max_length=255, unique=True)
    Author = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True, blank=True)
    Publication =  models.DateTimeField(auto_now=True)
    Content = models.TextField()

    class Meta:
        verbose_name = 'Blog'
        verbose_name_plural = 'Blog'
    
    def __int__(self):
        return self.id