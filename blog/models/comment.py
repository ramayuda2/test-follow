from django.db import models
from account.models.Account import Profile
from .Blog import Blog


class Comment(models.Model):
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE, null=True, blank=True)
    user = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True, blank=True)
    coment = models.TextField()

    def __str__(self):
        return self.coment