from django.test import TestCase
from account.models.Account import Profile
from blog.models.Blog import Blog

# Create your tests here.
class CreateProfile(TestCase):
    def setUp(self):
        Profile.objects.create(nama_lengkap='test', username='rama', password='1234', email='test@example.com')
        Profile.objects.create(nama_lengkap='test2', username='rama2', password='1234', email='test@example.com')
        Profile.objects.create(nama_lengkap='test3', username='rama3', password='1234', email='test@example.com')
    def test_create_profile(self):
        self.assertEqual(Profile.objects.all().count(), 3)