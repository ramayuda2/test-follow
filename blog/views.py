from django.http import JsonResponse
from django.shortcuts import redirect, render
from .models.Blog import Blog
from .models.comment import Comment
from account.models.Account import Folower, Profile
from django.core.files.storage import FileSystemStorage

# Create your views here.
def ProfileView(request, id):
    if request.method == 'POST':
        user = Profile.objects.get(id=id)
        file = request.FILES['file']
        print(file)

        save_file = FileSystemStorage()
        path = 'static/'+ file.name
        save_file.save(path, file)
        
        user.foto_profile = path
        user.save()
    return render(request, 'profil.html')

def HomeView(request):
    blog = Blog.objects.all()
    flw = Folower.objects.filter(User__id=request.user.id)

    if request.method == 'POST':
        user = Profile.objects.get(id=request.user.id)
        user_to_follow = Profile.objects.get(id=request.POST.get('id'))
        
        for i in flw:
            if i.follow.id == int(request.POST.get('id')):
                return JsonResponse({'data': 'User sudah difollow'})

        save = Folower(
            User = user,
            follow = user_to_follow
        )
        save.save()
        return JsonResponse({'data': 'User terfollow'})
    return render(request, 'index.html', {'ctx': blog, 'flw': flw, 'total': flw.count()})

def delete_post(request, id):
    if request.method == 'DELETE':
        find = Blog.objects.get(id=id)
        if find.Author.id == request.user.id:
            find.delete()
            return JsonResponse({'data': 'success'})
        else:
            return JsonResponse({'data': 'gagal'})

def edit_post(request, id):
    blog = Blog.objects.get(id=id)
    if request.method == 'POST':
        find = Blog.objects.get(id=id)
        find.Title = request.POST.get('title')
        find.Content= request.POST.get('summernote')
        find.save()
    return render(request, 'EditBlog.html', {'ctx': blog})

def CreateNewPostView(request):
    if request.user.is_authenticated == False:
        return redirect('/login')
    if request.method == 'POST':
        try:
            save = Blog(
                Title=request.POST.get('title'),
                Author=request.user,
                Content=request.POST.get('summernote'),
            )
            save.save()
            data = 'success'
        except Exception:
            data = 'fail'
        return JsonResponse(data, safe=False)
    return render(request, 'CreateBlogPost.html')


def DetailPostView(request, id):
    blog = ''
    if request.user.is_authenticated == False:
        return redirect('/login')
    blog = Blog.objects.get(id=id)
    try:
        comment = Comment.objects.filter(blog_id=blog.id)
    except Exception:
        comment = ''
    
    if request.method == 'POST':
        save = Comment(
            blog=blog,
            user=request.user,
            coment=request.POST.get('comment')
        )
        save.save()
        return redirect(f'/Detail-post/{blog.id}')

    if blog.Author.id == request.user.id:
        return render(request, 'BlogPostDetail.html', { 'ctx': blog, 'comment': comment })

    flow = Folower.objects.filter(User__id=request.user.id).values()
    for i in flow:
        if i['follow_id'] == blog.Author.id:
            return render(request, 'BlogPostDetail.html', { 'ctx': blog, 'comment': comment })
    return redirect('/')