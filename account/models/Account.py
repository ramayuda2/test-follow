from django.db import models
from django.contrib.auth.models import User, AbstractUser
# Create your models here.    

class Profile(AbstractUser):
    first_name 		= None
    last_name 		= None
    nama_lengkap 	= models.CharField(max_length=255,null=True)
    foto_profile = models.FilePathField(null=True, blank=True)
    class Meta:
        verbose_name = 'Profile'
        verbose_name_plural = 'Profile'

    def __str__(self):
        return self.username

class Folower(models.Model):
    User = models.ForeignKey(Profile, related_name='Profile', on_delete=models.CASCADE, null=True, blank=True)
    follow = models.ForeignKey(Profile,related_name='Folower_user_nama_lengkap' , on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        verbose_name = 'folower'
        verbose_name_plural = 'folower'
    
    def __int__(self):
        return self.User
