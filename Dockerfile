FROM python:3.11-slim-bullseye
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /blog

COPY . .

RUN apt-get update && \
    apt-get install -y \
        build-essential \
        make \
        g++ \
        gcc \
        libxslt-dev \
        postgresql-client \
        libpq-dev \
        musl-dev \
    && pip install --upgrade pip \
    && pip install -r requirements.txt \
    && apt-get remove -y --purge make gcc build-essential \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*

